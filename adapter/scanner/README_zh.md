# 扫描工具使用说明

## 简介

app_check_tool用于扫描指定路径的hap、hsp、app包，统计重复文件、统计超出指定大小的文件、统计各类型文件大小占比，输出html结果文件和json结果文件。

### 扫描工具系统架构图

![](image/construct.png)

### 1. 扫描工具使用指令

#### 1.1 示例

- 统计重复文件命令示例：


```
java -jar app_check_tool.jar --input <option> --out-path <option> --stat-duplicate [option]
```

- 统计超出指定大小的文件命令示例：


```
java -jar app_check_tool.jar --input <option> --out-path <option> --stat-file-size [option]
```

- 统计各类型文件大小占比命令示例：


```
java -jar app_check_tool.jar --input <option> --out-path <option> --stat-suffix [option]
```

- 统计重复文件、指定大小的文件、各类型文件大小占比命令示例：


```
java -jar app_check_tool.jar --input <option> --out-path <option> --stat-duplicate [option] --stat-file-size [option] --stat-suffix [option]
```

#### 1.2 参数含义及规范


| 指令               | 是否必选项 | 选项                   | 描述                                                         | 备注       |
|-----------------|-------|----------------------|---------------------------------------------------------------------|------------|
| --input          | 是     | NA                   | 指定传入的hap、hsp、app包文件路径                                    | NA         |
| --out-path       | 是     | NA                   | 指定结果输出文件夹目录                                               | NA         |
| --stat-duplicate | 否     | true或者false        | 默认参数值为false，如果为true <br>表示统计重复文件开启                 | NA         |
| --stat-file-size | 否     | NA                   | 默认最小单位为KB <br>表示统计超过设定大小的文件                        | int/KB     |
| --stat-suffix    | 否     | true或者false        | 默认参数值为false，如果为true <br>表示统计各类型（后缀名）文件占比开启   | NA         |




### 2. html格式化输出



#### 2.1 统计重复文件结果展示


<div id="box">
        <table>
            <tr>
                <td>taskType</td>
                <td>1</td>
            </tr>
            <tr>
                <td>taskDesc</td>
                <td>find the duplicated files</td>
            </tr>
            <tr>
                <td>param</td>
                <td>--stat-duplicate</td>
            </tr>
            <tr>
                <td>startTime</td>
                <td>2023-11-17 14:48:01:265</td>
            </tr>
            <tr>
                <td>stopTime</td>
                <td>2023-11-17 14:48:01:434</td>
            </tr>
            <tr>
                <td>result</td>
                <td>
                    <ul>
                        <li>
                            <table>
                                <tr>
                                    <td>md5</td>
                                    <td>975c41f5727b416b1ffefa5bb0f073b</td>
                                </tr>
                                <tr>
                                    <td>size</td>
                                    <td>1108880</td>
                                </tr>
                                <tr>
                                    <td>files</td>
                                    <td>
                                        <ul>
                                            <li>
                                                /home/admin/1116Test/duplicate/entry-default.hap/libs/example.so
                                            </li>
                                            <li>
                                                /home/admin/1116Test/duplicate/application-entry-default.hap/libs/example.so
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                </td>
            </tr>
        </table>
</div>


#### 2.2 统计超出指定大小的文件结果展示

<div id="box">
        <table>
            <tr >
                <td>taskType</td>
                <td>2</td>
            </tr>
            <tr>
                <td>taskDesc</td>
                <td>find files whose size exceed the limit size</td>
            </tr>
            <tr>
                <td>param</td>
                <td>--stat-file-size 4096</td>
            </tr>
            <tr>
                <td>startTime</td>
                <td>2023-11-17 14:48:01:458</td>
            </tr>
            <tr>
                <td>stopTime</td>
                <td>2023-11-17 14:48:01:491</td>
            </tr>
            <tr>
                <td>result</td>
                <td>
                    <table>
                        <tr>
                            <td>file</td>
                            <td>size</td>
                        </tr>
                        <tr>
                            <td>
                                /home/admin/1116Test/fileSize/application-entry-default.hap/libs/x86_64/example.so
                            </td>
                            <td>1292840</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
</div>

#### 2.3 统计各类型文件大小占比结果展示

<div id="box">
    <body>
        <table>
            <tr>
                <td>taskType</td>
                <td>3</td>
            </tr>
            <tr>
                <td>taskDesc</td>
                <td>show files group by file type[.suffix]</td>
            </tr>
            <tr>
                <td>param</td>
                <td>--stat-suffix</td>
            </tr>
            <tr>
                <td>startTime</td>
                <td>2023-11-17 14:48:01:497</td>
            </tr>
            <tr>
                <td>stopTime</td>
                <td>2023-11-17 14:48:01:537</td>
            </tr>
			<tr>
				<td>pathList</td>
				<td>
					/home/admin/1116Test/temporary/application-entry-default.hap<br>/home/admin/1116Test/temporary/entry-default.hap<br>
				</td>
			</tr>
            <tr>
                <td>result</td>
                <td>
                    <ul>
                        <li>
                            <table>
                                <tr>
                                    <td>suffix</td>
                                    <td>so</td>
                                </tr>
                                <tr>
                                    <td>totalSize</td>
                                    <td>1292840</td>
                                </tr>
                                <tr>
                                    <td>files</td>
                                    <td>
                                        compress:false<br>size:1292840<br>file:/home/admin/1116Test/suffix/application-entry-default.hap/libs/x86_64/example.so
                                    </td>
                                </tr>
                            </table>
                        </li>
						<li>
                            <table>
                                <tr>
                                    <td>suffix</td>
                                    <td>abc</td>
                                </tr>
                                <tr>
                                    <td>totalSize</td>
                                    <td>84852</td>
                                </tr>
                                <tr>
                                    <td>files</td>
                                    <td>
                                        size:8548<br>file:/home/admin/1116Test/suffix/entry-default.hap/ets/modules.abc<br>size:76304<br>file:/home/admin/1116Test/suffix/application-entry-default.hap/ets/modules.abc<br>
                                    </td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                </td>
            </tr>
        </table>
	</body>
</div>

#### 2.4 html详细字段信息

| 字段            | 类型                | 描述                                                  | 备注 |
| ----------------|--------------------|------------------------------------------------------| ---- |
| taskType        | int                | 任务类型 <br> 1 - 统计重复文件  <br> 2 - 统计超出指定大小的文件 <br> 3 - 统计各类型文件大小占比 | NA  |
| taskDesc        | String             | 任务的详细描述                                                                             | NA   |
| param           | String             | 扫描程序传入参数                                                                           | NA   |
| startTime       | String             | 程序开始执行时间                                                                           | NA   |
| stopTime        | String             | 程序结束时间                                                                               | NA   |
| pathList        | Vector\<String>    | 多个hap、hsp包的路径<br>仅在统计各类型文件占比<br>功能开启时且有多个hap、hsp包时展示            | NA   |
| result          |  NA                | 根据taskType返回对应分析结果的详细信息                                                       | NA   |

##### 3.4.1 统计重复文件字段信息

| 字段            | 类型               | 描述                                     | 备注 |
| ----------------| ------------------ |----------------------------------------| ---- |
| md5             | String             | 相同文件的MD5值                        | NA   |
| size            | int                | 相同文件的大小                         | NA   |
| files           | String             | 相同文件名的对应路径                     | NA   |

##### 3.4.2 统计超出指定大小的文件字段信息

| 字段            | 类型               | 描述                                     | 备注 |
| --------------- | ------------------ |----------------------------------------| ---- |
| file           | String             | 扫描的大文件的对应路径             | NA   |
| size            | int               | 扫描的大文件的对应大小             | NA   |

##### 3.4.3 统计各类型文件大小占比字段信息

| 字段            | 类型               | 描述                                     | 备注 |
| ----------------| ------------------ |----------------------------------------| ---- |
| suffix          | String             | 同类型文件后缀名                        | NA   |
| totalSize       | int                | 扫描的同类型<br>文件的总大小             | NA   |
| files           | NA                 | 扫描的同类型<br>文件的对应路径和大小      | NA   |

###### 3.4.3.1 同类型文件的对应路径和大小字段信息
| 字段            | 类型               | 描述                                     | 备注 |
| ----------------| ------------------ |----------------------------------------| ---- |
| file           | String    | 文件路径                                          | NA   |
| size           | int       | 文件的大小                                         | NA   |
| compress       | bool      | 默认参数值为false，<br>如果为true表示该<br>类型文件是压缩文件  | 仅支持so<br>类型文件  |

### 3. json格式化输出

#### 3.1 统计重复文件结果展示

```
[{
	"param":"--stat-duplicate",
	"result":[{
		"files":[
			"/home/admin/1116Test/duplicate/application-entry-default.hap/libs/armeabi-v7a/example.so",
			"/home/admin/1116Test/duplicate/entry-default.hap/libs/armeabi-v7a/example.so"
		],
		"md5":"975c41f5727b416b1ffefa5bb0f073b",
		"size":1108880
	}],
	"startTime":"2023-11-17 14:48:01:265",
	"stopTime":"",
	"taskDesc":"find the duplicated files",
	"taskType":1
}]
```

#### 3.2 统计超出指定大小的文件结果展示

```
[{
	"param":"--stat-file-size 4096",
	"result":[{
			"file":"/home/admin/1116Test/fileSize/application-entry-default.hap/libs/x86_64/example.so",
			"size":1292840
	}],
	"startTime":"2023-11-17 14:48:01:458",
	"stopTime":"2023-11-17 14:48:01:491",
	"taskDesc":"find files whose size exceed the limit size",
	"taskType":2
}]
```

#### 3.3 统计各类型文件大小占比结果展示

```
[{
	"param":"--stat-suffix",
	"pathList":[
		"/home/admin/1116Test/temporary/application-entry-default.hap",
		"/home/admin/1116Test/temporary/entry-default.hap"
	],
	"result":[{
		"files":[{
			"compress":"false",
			"file":"/home/admin/1116Test/suffix/application-entry-default.hap/libs/x86_64/example.so",
			"size":1292840
		}],
		"suffix":"so",
		"totalSize":1292840
	},
	{
		"files":[{
			"file":"/home/admin/1116Test/suffix/application-entry-default.hap/ets/modules.abc",
			"size":76304
		},
		{
			"file":"/home/admin/1116Test/suffix/entry-default.hap/ets/modules.abc",
			"size":8548
		}],
		"suffix":"abc",
		"totalSize":84852
	}],
	"startTime":"2023-11-17 14:48:01:497",
	"stopTime":"2023-11-17 14:48:01:537",
	"taskDesc":"show files group by file type[.suffix]",
	"taskType":3
}]
```

#### 3.4 json详细字段信息

| 字段            | 类型                | 描述                                                  | 备注 |
| ----------------|--------------------|------------------------------------------------------| ---- |
| taskType        | int                | 任务类型 <br> 1 - 统计重复文件  <br> 2 - 统计超出指定大小的文件 <br> 3 - 统计各类型文件大小占比 | NA  |
| taskDesc        | String             | 任务的详细描述                                                                             | NA   |
| param           | String             | 扫描程序传入参数                                                                           | NA   |
| startTime       | String             | 程序开始执行时间                                                                           | NA   |
| stopTime        | String             | 程序结束时间                                                                               | NA   |
| pathList        | Vector\<String>    | 多个hap、hsp包的路径<br>仅在统计各类型文件占比<br>功能开启时且有多个hap、hsp包时展示            | NA   |
| result          |  NA                | 根据taskType返回对应分析结果的详细信息                                                       | NA   |

##### 3.4.1 统计重复文件字段信息

| 字段            | 类型               | 描述                                     | 备注 |
| ----------------| ------------------ |----------------------------------------| ---- |
| md5             | String             | 相同文件的MD5值                        | NA   |
| size            | int                | 相同文件的大小                         | NA   |
| files           | String             | 相同文件名的对应路径                     | NA   |

##### 3.4.2 统计超出指定大小的文件字段信息

| 字段            | 类型               | 描述                                     | 备注 |
| ----------------| ------------------ |----------------------------------------| ---- |
| file            | String             | 扫描的大文件的对应路径                 | NA   |
| size            | int                | 扫描的大文件的对应大小                  | NA   |

##### 3.4.3 统计各类型文件大小占比字段信息

| 字段            | 类型               | 描述                                     | 备注 |
| ----------------| ------------------ |----------------------------------------| ---- |
| suffix          | String             | 同类型文件后缀名                         | NA   |
| totalSize       | int                | 扫描的同类型<br>文件的总大小           | NA   |
| files           | NA                 | 扫描的同类型<br>文件的对应路径和大小    | NA   |

###### 3.4.3.1 同类型文件的对应路径和大小字段信息
| 字段            | 类型               | 描述                                     | 备注 |
| ----------------| ------------------ |----------------------------------------| ---- |
| file           | String             | 文件路径                                 | NA   |
| size           | int                | 文件的大小                               | NA   |
| compress       | bool               | 默认参数值为false，<br>如果为true表示该<br>类型文件是压缩文件  | 仅支持so<br>类型文件  |